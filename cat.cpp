///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   22 April 2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

bool CatEmpire::empty() {
   return (topCat == nullptr);
}

void CatEmpire::addCat(Cat* newCat) {
   if(topCat == nullptr) {
      topCat = newCat;
      return;
   } 
   
   addCat(topCat, newCat);
}

void CatEmpire::addCat(Cat* atCat, Cat* newCat) {
   //assert(newCat->name != atCat->name);

   if(atCat->name > newCat->name) {
      if(atCat->left == nullptr) {
         atCat->left = newCat;
      } else {
         addCat(atCat->left, newCat);
      }
   } else {
      if(atCat->right == nullptr) {
         atCat->right = newCat;
      } else {
         addCat(atCat->right, newCat);
      }
   }
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorderReverse( topCat, 1 );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
   if(atCat->right != nullptr) {
      dfsInorderReverse(atCat->right, depth + 1);
   }

   cout << string(6 * (depth - 1), ' ') << atCat->name;
   
   if((atCat->left == nullptr) && (atCat->right == nullptr)) {
      cout << endl;
   } else if((atCat->left != nullptr) && (atCat->right != nullptr)) {
      cout <<  "<" << endl;
   } else if(atCat->left != nullptr) {
      cout << "\\" << endl;
   } else if(atCat->right != nullptr) {
      cout << "/" << endl;
   }

   if(atCat->left != nullptr) {
      dfsInorderReverse(atCat->left, depth + 1);
   }

}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorder( topCat );
}

void CatEmpire::dfsInorder( Cat* atCat ) const {
   if(atCat->left != nullptr) {
      dfsInorder(atCat->left);
   }

   cout << atCat->name << endl;

   if(atCat->right != nullptr) {
      dfsInorder(atCat->right);
   }
}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsPreorder( topCat );
}

void CatEmpire::dfsPreorder( Cat* atCat ) const {
   if((atCat->left == nullptr) && (atCat->right == nullptr)) {
      return;
   } else if((atCat->left != nullptr) && (atCat->right != nullptr)) {
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   } else if(atCat->left != nullptr) {
      cout << atCat->name << " begat " << atCat->left->name << endl;
   } else if(atCat->right != nullptr) {
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }

   if(atCat->left != nullptr) {
      dfsPreorder(atCat->left);
   }

   if(atCat->right != nullptr) {
      dfsPreorder(atCat->right);
   }
}

string getEnglishSuffix(int n) {
   if((n == 11) || (n == 12) || (n ==13)) {
      return "th";
   } else if(n % 10 == 1) {
      return "st";
   } else if(n % 10 == 2) {
      return "nd";
   } else if(n % 10 == 3) {
      return "rd";
   } else {
      return "th";
   }
}

void CatEmpire::catGenerations() const {
   if( topCat == nullptr ) {
      cout << "No cats!" << endl;
      return;
   }

   bfSearch(topCat);
}

void CatEmpire::bfSearch(Cat* atCat) const {
   queue<Cat*> catQueue;
   int depth = 1;
   catQueue.push(nullptr);
   catQueue.push(atCat);
   while(catQueue.size() > 1) {
      Cat* cat = catQueue.front();
      catQueue.pop();

      if(cat == nullptr) {
         cout << depth << getEnglishSuffix(depth) << " Generation" << endl;
         depth++;
         catQueue.push(nullptr);
      } else {

      cout << "  " << cat->name;

      if(catQueue.front() == nullptr) {
         cout << endl;
      }

      if(cat->left != nullptr) {
         catQueue.push(cat->left);
      }

      if(cat->right != nullptr) {
         catQueue.push(cat->right);
      }
      }

   }
}

bool CatEmpire::validate() {
   if(empty()) {
      assert(topCat == nullptr);
      assert(topCat->left == nullptr);
      assert(topCat->left == nullptr);
      return true;
   } else {
      assert(topCat != nullptr);
      assert(validate(topCat));
      return true;
   }
}

bool CatEmpire::validate(Cat* atCat) {
   if(atCat == nullptr){
      assert(atCat->left == nullptr);
      assert(atCat->left == nullptr);
      return true;
   }

   if(atCat->left != nullptr) {
      assert(atCat->name > atCat->left->name);
      validate(atCat->left);
   }

   if(atCat->right != nullptr) {
      assert(atCat->right->name > atCat->name);
      validate(atCat->right);
   }

   return true;
}

void CatEmpire::dump() {
   if(empty()) {
      return;
   } else {
      cout << "CatEmpire:  topCat = [" << topCat->name << "]" << endl;
      dump(topCat);
   }
}

void CatEmpire::dump(Cat* atCat) {
   if(atCat == nullptr) {
      return;
   } else {
      cout << "Current Cat = [" << atCat->name << "]" << endl;
   }

   if(atCat->left == nullptr && atCat->right == nullptr) {
      cout << "   No child cats" << endl;
   }

   if(atCat->left != nullptr) {
      cout << "   Left child of " << atCat->name << " = [" << atCat->left->name << "]" << endl;
   }

   if(atCat->right != nullptr) {
      cout << "   Right child of " << atCat->name << " = [" << atCat->right->name << "]" << endl;
   }
   
   dump(atCat->left);
   dump(atCat->right);
}














